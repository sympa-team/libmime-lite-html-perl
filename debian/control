Source: libmime-lite-html-perl
Section: perl
Priority: optional
Build-Depends: debhelper (>= 11)
Build-Depends-Indep:
    perl,
    libwww-perl,
    libhtml-parser-perl,
    liburi-perl,
    libmime-lite-perl (>= 1)
Maintainer: Debian Sympa Team <libmime-lite-html-perl@packages.debian.org>
Uploaders:
    Stefan Hornburg (Racke) <racke@linuxia.de>,
    Emmanuel Bouthenot <kolter@debian.org>
Standards-Version: 4.1.4
Homepage: https://metacpan.org/release/MIME-Lite-HTML
Vcs-Git: https://salsa.debian.org/sympa-team/libmime-lite-html-perl.git
Vcs-Browser: https://salsa.debian.org/sympa-team/libmime-lite-html-perl

Package: libmime-lite-html-perl
Architecture: all
Depends:
    ${perl:Depends},
    ${misc:Depends},
    libwww-perl,
    libhtml-parser-perl,
    liburi-perl,
    libmime-lite-perl (>= 1)
Description: Transform HTML page into MIME email
 Mime::Lite::Html provides an interface for sending message that supports HTML
 format and builds it for you. This is useful to transform HTML pages into MIME
 email like that:
 .
  * Get the page with LWP if needed
  * Parse page to find included images and objects (gif, jpg, flash)
  * Attach them to mail with proper header
  * Include external CSS and Javascript files
  * Replace relative urls with absolute ones
  * Build the MIME email with any parts found
